FROM python:3

WORKDIR /app

COPY . .

RUN pip3 install -r requirements.txt

EXPOSE 5000

ENV FLASK_ENV=Development
ENV TEST_ADMIN_LOGIN=admin
ENV TEST_ADMIN_PASSWORD=admin

CMD [ "python", "./run.py"]