from produceAPI import app, logger, log_msg
from flask import render_template, url_for, request


@app.get('/')
def index():
    """Index  welcome page"""
    with open('./README', 'r') as file:
        body = file.read().replace('\n', '<br>')
    logger.info(log_msg(request, "Index page accessed"))
    return render_template("index.html",
                           title="Flask REST API Example",
                           body=body)


@app.errorhandler(404)
def not_found(error):
    """404 page"""
    logger.error(log_msg(request, str(error)))
    return render_template("index.html",
                           title="Unable to find page you are looking for",
                           body=(f"Please visit  <a href='{url_for('index')}'>"
                                 f"Main page</a> for details"))


@app.errorhandler(405)
def method_not_allowed(error):
    """405 page"""
    logger.error(log_msg(request, str(error)))
    return render_template("index.html",
                           title="HTTP method is not allowed for this page",
                           body=(f"Please visit  <a href='{url_for('index')}'>"
                                 f"Main page</a> for details"))