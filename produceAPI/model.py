from produceAPI import db


class Post(db.Model):
    """This class defines structure of Posts table"""
    id = db.Column(db.Integer, primary_key=True)
    author = db.Column(db.String(80), unique=False, nullable=False)
    title = db.Column(db.String(80), unique=True, nullable=False)
