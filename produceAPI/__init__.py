import logging
from os import path
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc


# Configure FLASK and SQLAlchemy
app = Flask(__name__)
app.config.from_pyfile("settings.py")
db = SQLAlchemy(app)

# Configure logging
logger = logging.getLogger(__name__)
logger.setLevel(app.config["LOGGING_LEVEL"])
formatter = logging.Formatter(app.config["LOGGING_FORMAT"])
file_handler = logging.FileHandler(app.config["LOGGING_TARGET_FILE"])
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


def log_msg(request, message):
    """This function returns pretty format for log messages"""
    return (f" {str(request.remote_addr)} {str(request.user_agent)}"
            f" {str(request.method)} {message}")

# Configure Swagger
from produceAPI.swagger import SWAGGER_BLUEPRINT, SWAGGER_URL
app.register_blueprint(SWAGGER_BLUEPRINT, url_prefix=SWAGGER_URL)

# Import models and routes
from produceAPI.model import Post
import produceAPI.routes_html
import produceAPI.routes_rest


# Create database and initialize Post table if it is not exist yet
logger.info("Initializing DB")
if not path.exists("./produceAPI/data.db"):
    logger.info("Create new DB since its not exist")
    db.create_all()
    # Pre-populate database with few rows
    post1 = Post(author="Sergey Kiyan", title="Python ORM")
    post2 = Post(author="Sergey Kiyan", title="Python restAPI")
    db.session.add_all([post1, post2])
    try:
        db.session.commit()
    except exc.IntegrityError:
        logger.warning("Post already exist")
    except Exception as err:
        logger.critical(str(err))
        raise err
else:
    logger.info("DB already exist, ok to go")
