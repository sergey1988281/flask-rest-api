import jwt
from produceAPI import app, db, logger, log_msg
from sqlalchemy import exc
from produceAPI.model import Post
from flask import request, make_response
from datetime import datetime, timedelta
from werkzeug.security import check_password_hash
from functools import wraps


def token_required(f):
    """This decorator ensure only requests with a vaid token are processed"""
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if "x-access-token" in request.headers:
            token = request.headers["x-access-token"]
        if not token:
            logger.error(log_msg(request, "Accessed without valid token"))
            return {"status": "Failed", "details": "No token"}, 401
        try:
            data = jwt.decode(token, app.config["SECRET_KEY"],
                              algorithms=["HS256"])
        except Exception as e:
            logger.error(log_msg(request, "Token " + str(e)))
            return {"status": "Failed", "details": "Token " + str(e)}, 401
        if data["username"] != app.config["TEST_ADMIN_LOGIN"]:
            logger.error(log_msg(request, "Token has invalid payload data"))
            return {"status": "Failed",
                    "details": "Token has invalid payload data"}, 401
        return f(*args, **kwargs)
    return decorated


@app.get("/posts")
@token_required
def get_posts():
    """Get all posts data"""
    posts = Post.query.all()
    output = []
    for post in posts:
        post_data = {"id": post.id,
                     "author": post.author,
                     "title": post.title}
        output.append(post_data)
    logger.info(log_msg(request, "All post data was requested"))
    return {"posts": output}


@app.get("/posts/<id>")
@token_required
def get_post_data_by_id(id):
    """Get specific post data by ID"""
    post = Post.query.get(id)
    if not post:
        logger.warning(log_msg(request,
                               (f"Post with id:{str(id)} data "
                                f"was requested but not exist")))
        return {"status": "Failed",
                "details": f"Post with id:{str(id)} does not exist"}, 404
    logger.info(log_msg(request, f"Post with id:{str(id)} data was requested"))
    return {"author": post.author, "title": post.title}


@app.post("/posts")
@token_required
def add_post_data():
    """Add new post data and return its ID back"""
    post = Post(author=request.json["author"],
                title=request.json["title"])
    db.session.add(post)
    try:
        db.session.commit()
    except exc.IntegrityError:
        logger.warning(log_msg(request,
                               (f"Post with title {request.json['title']}"
                                f" already exist")))
        return {"status": "Failed",
                "details": (f"Post with title {request.json['title']}"
                            f" already exist")}, 409
    except Exception as err:
        logger.error(log_msg(request, str(err)))
        return {"status": "Failed",
                "details": "Unknown error, please check logs"}, 400
    logger.info(log_msg(request, f"Post with id:{str(post.id)} added"))
    return {"status": "Success", "id": str(post.id)}


@app.put("/posts/<id>")
@token_required
def update_post_data(id):
    """Update existing post data and return it back"""
    post = Post.query.get(id)
    if not post:
        logger.warning(log_msg(request,
                               (f"Post with id:{str(id)} was requested to"
                                f" update but not exist")))
        return {"status": "Failed",
                "details": f"Post with id:{str(id)} does not exist"}, 404
    post.author = request.json["author"]
    post.title = request.json["title"]
    try:
        db.session.commit()
    except exc.IntegrityError:
        logger.warning(log_msg(request,
                               (f"Post with title {request.json['title']}"
                                f" already exist")))
        return {"status": "Failed",
                "details": "Post with such title already exist"}, 409
    except Exception as err:
        logger.error(log_msg(request, str(err)))
        return {"status": "Failed",
                "details": "Unknown error, please check logs"}, 400
    logger.info(log_msg(request, f"Post with id:{str(id)} updated"))
    return {"status": "Success",
            "id": str(id),
            "author": post.author,
            "title": post.title}


@app.delete("/posts/<id>")
@token_required
def delete_post_data_by_id(id):
    """Delete specific post data by ID"""
    post = Post.query.get(id)
    if not post:
        logger.warning(log_msg(request,
                               (f"Post with id:{str(id)} was requested to"
                                f" delete but not exist")))
        return {"status": "Failed",
                "details": f"Post with id:{str(id)} does not exist"}, 404
    db.session.delete(post)
    try:
        db.session.commit()
    except Exception as err:
        logger.error(log_msg(request, str(err)))
        return {"status": "Failed",
                "details": "Unknown error, please check logs"}, 400
    logger.info(log_msg(request, f"Post with id:{str(id)} deleted"))
    return {"status": "Success", "details": f"Post with id:{str(id)} deleted"}


@app.get("/login")
def get_token():
    """Performs basic HTTP authentification and returns token"""
    auth = request.authorization
    if not auth or not auth.username or not auth.password:
        logger.error(log_msg(request,
                             "No valid authentification data was provided"))
        return make_response(
                "Could not verify",
                401,
                {"WWW-Authenticate": "Basic realm = 'Login required!'"})
    if auth.username != app.config["TEST_ADMIN_LOGIN"]:
        logger.error(log_msg(request, "Wrong username was provided"))
        return make_response(
                "Username is invalid",
                401,
                {"WWW-Authenticate": "Basic realm = 'Login required!'"})
    if not check_password_hash(app.config["TEST_ADMIN_PASSWORD"],
                               auth.password):
        logger.error(log_msg(request, "Password was invalid"))
        return make_response(
            "Password is invalid",
            401,
            {"WWW-Authenticate": "Basic realm = 'Login required!'"})
    token = jwt.encode({"username": app.config["TEST_ADMIN_LOGIN"],
                        "exp": datetime.utcnow() + timedelta(minutes=30)},
                       app.config["SECRET_KEY"],
                       algorithm="HS256")
    logger.info(log_msg(request, "New token was issued"))
    return {"Token": token}
