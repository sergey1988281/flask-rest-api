from os import environ
from werkzeug.security import generate_password_hash
import logging


SQLALCHEMY_DATABASE_URI = "sqlite:///data.db"
SQLALCHEMY_TRACK_MODIFICATIONS = False
SECRET_KEY = "192b9bdd22ab9ed4d12e236c78afcb9a393ec15f71bbf5dc987d54727823bcbf"
TEST_ADMIN_LOGIN = environ["TEST_ADMIN_LOGIN"]
TEST_ADMIN_PASSWORD = generate_password_hash(environ["TEST_ADMIN_PASSWORD"],
                                             method="sha256")
LOGGING_LEVEL = logging.INFO
LOGGING_FORMAT = "%(levelname)s:%(asctime)s:%(message)s"
LOGGING_TARGET_FILE = "produceAPI.log"
