from produceAPI import app

if __name__ == '__main__':
    # Run server
    app.run(debug=False, host="0.0.0.0")
